@extends('layouts.app')

@section('title', 'Astuces')
@section('content') 
    <section id="astuces">
        <div class="row">
            <div class="col-12 col-md-6">
                <h1>"On doit prendre soin de notre Planète comme de notre famille" : 10 tips écolo pour tous! </h1>
                <h4>Préserver la Planète, vous êtes pour. Mais au quotidien, vous ne savez pas trop comment vous y prendre. Cela vous paraît fastidieux, long, cher, ennuyeux ? Faux ! A l'occasion de la sortie du film "Sauver ou périr", Pierre Niney a pris les commandes d'aufeminin. Ecolo convaincu, il a souhaité évoqué avec nous son engagement éco-responsable pour une Planète plus verte. Et ce n'est vraiment, vraiment pas compliqué. La preuve par 10. </h4>  
            </div>
            <div class="col-12 col-md-6">
                <img class='img-astuce' src="img/astuces.png" alt="astuces">
            </div>
        </div>
    </section>
    <section id="tips">
        <ul>
            <li class="card" tabindex="0" aria-haspopup="true">
                <div class="intro">
                <img src="img/petitemaison.png" width="140" height="140" alt="maison flat design">
                <h2>À la maison</h2>
                </div>
                <div class="info">
                    <ol> 
                        <li>On ne surchauffe pas son intérieur. On installe un thermostat afin de maintenir une température de 18°C dans les chambres et 19°C dans les autres pièces. Rien qu’en baissant d’un seul degré la température, on réalise 7% d’économie sur sa consommation.</li><br/>
                        <li>On pense à éteindre la lumière lorsqu’on quitte une pièce et on fait de même avec les appareils (télévision, lecteur CD-DVD, ordinateur…). Ils consomment 10 % d’électricité en plus lorsqu’ils sont en veille.</li><br/>
                        <li> On ferme le robinet lorsqu’on se brosse les dents pour ne pas gâcher 10 000 litres d’eau par an ! Un geste écologique très facile à réaliser.</li>
                    </ol>
                </div>
            </li>
            <li class="card" tabindex="0" aria-haspopup="true">
                <div class="intro">
                <img src="img/magasin.png" width="140" height="140" alt="magasin flat design">
                <h2>Au magasin</h2>
                </div>
                <div class="info">
                    <ol> 
                        <li>On ne rechigne pas à pratiquer le « tri sélectif » en séparant bien ses ordures ménagères. Pour ne pas se tromper de couleur de bac </li><br/>
                        <li>On utilise des petits cabas pliables et réutilisables pour faire ses courses. On boude les sacs en plastique qui mettent plus de 400 ans à se détruire dans la nature. En plus, les tote bag, c'est tendance !</li><br/>
                        <li>On achète des couches lavables ! Une couche jetable = 500 ans pour se dégrader et un bébé utilise au moins 5000 couches traditionnelles avant d’être propre.</li><br/>
                    </ol>
                </div>
            </li>
            <li class="card" tabindex="0" aria-haspopup="true">
                <div class="intro">
                <img src="img/travail.png" width="140" height="140" alt="travail flat design">
                <h2>Au travail</h2>
                </div>
                <div class="info">
                    <ol> 
                        <li>On imprime le moins possible les documents et on n’hésite pas exploiter le verso des feuilles déjà utilisées (pour communication interne, pages de brouillon…).</li><br/>
                        <li>On demande à chacun de venir avec son petit mug personnel et on dit adieu aux gobelets en plastique pour prendre son café. Une idée écologique et une bonne occasion d'avoir une jolie tasse sur son bureau.</li><br/>
                    </ol>
                </div>
            </li>
            <li class="card" tabindex="0" aria-haspopup="true">
                <div class="intro">
                <img src="img/transport.png" width="140" height="140" alt="transport flat design">
                <h2>Dans les transports</h2>
                </div>
                <div class="info">
                    <ol> 
                        <li>On choisit le train pour faire de longs trajets et le bus pour les trajets quotidiens</li><br/>
                        <li>On utilise le moyen de transport le plus écologique pour parcourir de petites distances : ses pieds. La marche, c’est bon pour la santé et pour la planète !</li>
                    </ol>
                </div>
            </li>
            </ul>
        </section>
        </body>
    </section>

@endsection


