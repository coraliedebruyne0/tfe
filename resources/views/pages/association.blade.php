@extends('layouts.app')

@section('title', 'Associations')
@section('content')
<section id="association">

    <div class="legende">
        <ul class="menu"> 
            <li class="menu_item">🌊 Nettoyer les Océans</li> 
            <li class="menu_item">🐝 Sauver la Biodiversité</li> 
            <li class="menu_item">🌳 Protéger terres et forêts</li> 
            <li class="menu_item">☀️ Développer les énergies propres</li> 
            <li class="menu_item">👪 Citoyenneté et Droits de l’Homme</li>
        </ul>
    </div>
    <div class="assoc">
        <div class="blbh_header">
            <img  src="https://dl.airtable.com/.attachmentThumbnails/d0625f281bce63b1dd2fdfd8868cede9/8d3304ca" alt="Be Love Be Happy" width="120" scale="0">
            <h3>Be Love Be Happy</h3>
            <p>Citoyenneté, Biodiversité, Terre et Forêt, Océan</p>
            <p>🌊🐝🌳👪</p>
        </div>
        <div class="blbh_detail" style="display: none;">
            <div>
                <div>
                    <ul class="detail">
                        <li><strong>Site Web : </strong>www.belovebehappy.com</li>
                        <li><strong>Présentation : </strong><p class="presentation">“Be Love Be Happy pour une Planète Heureuse” La nouvelle Communauté “Ecolo” du Bonheur qui fait du Bien et du Lien NOTRE VOCATION : UN mouvement “ECOLO” ? – Pour CREER une chaine du Coeur entre TOUS, l’individuel et les collectifs (Associations, Fondations, Ecoles et autres organismes culturels) – Refaire le LIEN en toute simplicité pour la sauvegarde de la Planète sur trois plans Humain, Environnemental et Animal Du BONHEUR ? Pour AGIR et porter #labonnenouvelle pour une meilleure Planète. Une Ecologie Engagée, Spirituelle, Heureuse et Positive !</p></li>
                        <li><strong>Téléphone : </strong>06 25 29 03 18</li>
                        <li><strong>Adresse mail : </strong> laurencedujardin@live.fr</li>
                        <li><strong>Adresse postale : </strong>7 rue souveraine </li>
                        <li><strong>Ville : </strong>1050 IXELLES BELGIQUE</li>
                        <li><strong>Année de fondation : </strong>2018</li>
                        <li><button class="soutenir"><a href="https://laurencedujardin.learnybox.com/adhesion-be-love-be-happy" target="_blank">Soutenir</a></li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="assoc">
        <div class="terre_header">
            <img  src="https://dl.airtable.com/.attachmentThumbnails/e6a5aa2f1c452d90c058a73ad077bee8/51b0bd0b" alt="Terre solidaire" width="120" scale="0">
            <h3>Fondation Terre Solidaire</h3>
            <p>Biodiversité, Terre et Forêt, Énergies propres, Citoyenneté</p>
            <p>🐝🌳☀️👪</p>
        </div>
        <div class="terre_detail" style="display: none;">
            <div>
                <div>
                    <ul class="detail">
                        <li><strong>Site Web : </strong>https://fondation-terresolidaire.org/</li>
                        <li><strong>Présentation : </strong><p class="presentation">Face à la crise écologique, économique et sociale, il est urgent de changer de modèle. Reconnue d’utilité publique et abritante, la Fondation Terre Solidaire identifie, soutient, et favorise la diffusion de projets audacieux, en France et à l'international, véritables alternatives à notre modèle de développement. Plus respectueux de la nature, des femmes et des hommes, ils inventent de nouvelles façons de produire, de consommer, et de vivre ensemble.</p></li>
                        <li><strong>Téléphone : </strong>01 44 82 80 80</li>
                        <li><strong>Adresse mail : </strong>contact@fondation-terresolidaire.org</li>
                        <li><strong>Adresse postale : </strong>18 rue Jean Lantier</li>
                        <li><strong>Ville : </strong>Paris</li>
                        <li><strong>Année de fondation : </strong>2016</li>
                        <li><button class="soutenir"><a href="https://soutenir.fondation-terresolidaire.org/b/mon-don" target="_blank">Soutenir</a></li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="assoc">
        <div class="greenpeace_header">
            <img  src="https://dl.airtable.com/.attachmentThumbnails/d570a8ee5e50b29ab9adf60ee20755ec/84734265" alt="Greenpeace" width="120" scale="0">
            <h3>Greenpeace</h3>
            <p>Citoyenneté, Biodiversité, Terre et Forêt, Énergies propres, Océan</p>
            <p>🌊🐝🌳☀️👪</p>
        </div>
        <div class="greenpeace_detail" style="display: none;">
            <div>
                <div>
                    <ul class="detail">
                        <li><strong>Site Web : </strong>www.greenpeace.fr</li>
                        <li><strong>Présentation : </strong><p class="presentation">Greenpeace est un réseau international d’organisations indépendantes qui agissent selon les principes de non-violence pour protéger l’environnement, la biodiversité et promouvoir la paix. Elle s’appuie sur un mouvement de citoyennes et citoyens engagé-e-s pour construire un monde durable et équitable. Greenpeace est présente dans 55 pays, sur tous les continents et tous les océans grâce à ses 28 bureaux nationaux et régionaux et ses trois bateaux. Elle compte plus de trois millions d’adhérent-e-s et plus de 36 000 bénévoles à travers le monde. Les campagnes de Greenpeace France s’articulent autour des principaux objectifs suivants: mettre en œuvre la [R]évolution énergétique pour faire face à la menace principale : les changements climatiques protéger nos océans de la surpêche et créer un réseau international de réserves marines préserver la richesse et la biodiversité des forêts primaires, lutter contre la déforestation et défendre les droits des communautés autochtones promouvoir une agriculture écologique et travailler à l’élimination des pesticides, des OGM et des produits toxiques qui nuisent à notre santé et à la biodiversité Enfin, depuis son origine, Greenpeace appelle au désarmement et à l’élimination des armes nucléaires, et aujourd’hui à protéger les droits des réfugiés et victimes des conflits ou dérèglements climatiques. Le pouvoir et la mobilisation des citoyennes et citoyens sont au cœur de notre travail et de notre méthode d’action. </p></li>
                        <li><strong>Téléphone : </strong>01 80 96 96 96</li>
                        <li><strong>Adresse mail : </strong>contact.fr@greenpeace.org</li>
                        <li><strong>Adresse postale : </strong>13, rue d’Enghien</li>
                        <li><strong>Ville : </strong>75010 PARIS</li>
                        <li><strong>Année de fondation : </strong>1971</li>
                        <li><button class="soutenir"><a href="https://faire-un-don.greenpeace.fr/" target="_blank">Soutenir</a></li>
                        
                    </ul>
                </div>
            </div>
        </div>
    </div>
    <div class="assoc">
        <div class="wwf_header">
            <img  src="https://dl.airtable.com/.attachmentThumbnails/c7ef214b90ec89d6c948dfcde0065a6c/b040b0a2" alt="WWF" width="120" scale="0">
            <h3>WWF France</h3>
            <p>Biodiversité, Terre et Forêt, Océan</p>
            <p>🌊🐝🌳</p>
        </div>
        <div class="wwf_detail" style="display: none;">
            <div>
                <ul class="detail">
                    <li><strong>Site Web : </strong>www.wwf.fr</li>
                    <li><strong>Présentation : </strong><p class="presentation">Le WWF signifie World Wide Fund for Nature ou en français, Fonds Mondial pour la Nature. C’est une ONG qui est dédiée à la protection de l’environnement, se trouve fortement impliquée dans les combats en faveur des développements durables. Son slogan est For a living planet. L’organisation travaille avec un nombre de plus d’une centaine de pays, et supporte à peu près 1300 projets en relation avec l’environnement.Les activités de WWF France La surveillance pour l’application de la réglementation nationale et internationale L’étude scientifique pour des diagnostics ou propositions Les restaurations d’espaces naturels qui se sont dégradés La formation, l’éducation ou la sensibilisation d’un public de tout âge au respect de l’environnement.</p></li>
                    <li><strong>Téléphone : </strong>01 71 86 43 08</li>
                    <li><strong>Adresse mail : </strong>sonia@wwf.fr</li>
                    <li><strong>Adresse postale : </strong>35, rue Baudin</li>
                    <li><strong>Ville : </strong>93310 Le Pré Saint Gervais</li>
                    <li><strong>Année de fondation : </strong>1961</li>
                    <li><button class="soutenir"><a href="https://faireundon.wwf.fr/LeCercle/~mon-don/" target="_blank">Soutenir</a></li>
                        
                </ul>
            </div>
        </div>
    </div>
</section> 
<script src="https://ajax.googleapis.com/ajax/libs/jquery/2.1.1/jquery.min.js"></script>
@endsection
