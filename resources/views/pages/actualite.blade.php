@extends('layouts.app')

@section('title', 'Actualité')
@section('content')
    <section id=actualité>
        <figure class="article hover">
            <img src="https://t3.llb.be/gJS1dU_4ZJ5XeIjf_rm10pFJ_-w=/0x229:1920x1189/940x470/5d5668a7d8ad58593579b08e.jpg" alt="écologie"/>
            <div class="date"><span class="day">18</span><span class="month">Aout</span></div>
            <figcaption>
                <h5>Écologie : un être humain n'est jamais de trop!</h5>
                <p>
                Ne pas faire d'enfants pour sauver la planète. Que certains puissent poser ce choix est évidemment sain et tout à fait respectable.
                </p>
                <button  target="_blank" rel="noopener noreferrer" onclick="window.open('https://www.lalibre.be/debats/opinions/ecologie-un-etre-humain-n-est-jamais-de-trop-5d566626d8ad5859357994d1')">Read More</button>
        </figure>
        <figure class="article hover">
            <img src="https://s3-us-west-2.amazonaws.com/s.cdpn.io/331810/sample66.jpg" alt="amazonie"/>
            <div class="date"><span class="day">24</span><span class="month">Aout</span></div>
            <figcaption>
                <h5>L’Amazonie en feu: Sa forêt? <br/>Nos poumons!
                </h5>
                <p>
                Face à la bêtise du président d'une des puissances mondiales et l’incapacité des autres de faire bloc, espérons...
                </p>
                <button  onclick="window.open('https://plus.lesoir.be/243706/article/2019-08-24/lamazonie-en-feu-sa-foret-nos-poumons')">Read More</button>
        </figure>
        <figure class="article hover">
            <img src="https://images4.persgroep.net/rcs/vjIBzHqUOcoIATIYmmh3S069628/diocontent/154831388/_fitwidth/694/?appId=21791a8992982cd8da851550a453bd7f&quality=0.9" alt="école"/>
            <div class="date"><span class="day">26</span><span class="month">Aout</span></div>
            <figcaption>
                <h5>Vingt-neuf écoles bruxelloises se lancent le zéro déchet</h5>
                <p>
                Vingt-neuf écoles bruxelloises se lanceront dès la rentrée dans un projet zéro déchet, annonce Bruxelles Environnement. 
                </p>
                <button onclick="window.open('https://www.7sur7.be/ecologie/vingt-neuf-ecoles-bruxelloises-se-lancent-dans-un-projet-zero-dechet~a2cb6298/')">Read More</button>
        </figure>
        <figure class="article hover">
            <img src="https://stock.rtl.lu/rtl/800/rtl2008.lu/nt/p/2019/08/26/17/dec8791851e6e635ce6e39886cae370b.jpeg" alt="foret"/>
            <div class="date"><span class="day">26</span><span class="month">Aout</span></div>
            <figcaption>
                <h5>Cinq façons d'aider VRAIMENT l'Amazonie!</h5>
                <p>
                Non, envoyer de l'argent n'est pas la seule solution. Il existe d'autres façons de soutenir l'Amazonie, sans même dépenser un rond ni sortir de son canapé!
                </p>
                <button onclick="window.open('https://5minutes.rtl.lu/actu/cinqbonnesraisons/a/1394610.html')">Read More</button>
        </figure>
        <figure class="article hover">
            <img src="https://www.sudinfo.be/sites/default/files/dpistyles_v2/ena_sp_16_9_illustration_principale/2019/08/26/node_137746/39354471/public/2019/08/26/B9720692516Z.1_20190826224742_000+G40EAN7F2.1-0.jpg?itok=FJHFDiRV" alt="canicule"/>
            <div class="date"><span class="day">26</span><span class="month">Aout</span></div>
            <figcaption>
                <h5>Une troisième canicule aujourd’hui en Belgique</h5>
                <p>
                Hier, aux alentours de 17 heures, la température a dépassé les 31 degrés à Uccle. Dimanche, le maximum enregistré au même endroit était de 30,9º. 
                </p>
                <button onclick="window.open('https://www.sudinfo.be/id137746/article/2019-08-27/une-troisieme-canicule-aujourdhui-en-belgique-ce-mardi-pourrait-atteindre-les-32')">Read More</button>
        </figure>
    
        <figure class="article hover">
            <img src="https://img-4.linternaute.com/DtNN3lbzY0Vhn60XO-3lObxKkLg=/540x/smart/e8df08cc628545b2b8dc881ca76bce91/ccmcms-linternaute/11513830.jpg" alt="canicule"/>
            <div class="date"><span class="day">28</span><span class="month">Aout</span></div>
            <figcaption>
                <h5>Entre canicules et vagues de chaleur</h5>
                <p>
                Avec déjà deux canicules en juin et en juillet 2019. La France connaît un été chaud et estival avec des températures dépassant les 30 degrés.
                </p>
                <button onclick="window.open('https://www.linternaute.com/bricolage/magazine/1469136-canicule-2019-canicules-et-vagues-de-chaleurs-un-ete-2019-chaud/')">Read More</button>
        </figure>
    </section>


@endsection
