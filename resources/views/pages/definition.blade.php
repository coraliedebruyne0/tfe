@extends('layouts.app')

@section('title', 'Definition')
@section('content')

<section id="but">
    <div class="container">
        <h2>L’écologie , Qu’est ce que c’est et à quoi ça sert ?</h2>
        <div class="row">
            <div class="col-12 col-md-8">
                <p>Le terme « écologie » vient des mots grecs "oikos" (la maison) et « logos » (discours, science, connaissance).  <br/><br/>
                Le mot écologie a été utilisé pour la première fois en 1866 par le biologiste Ernst Haeckel qui définie l’écologie comme «la science des relations des organismes avec le monde environnant ». L’écologie au sens large est donc la science qui étudie les conditions d’existence.  <br/> <br/> Depuis, sa définition a été précisée par le scientifique Dajos en 1983. La définition généralement admise est que l’écologie est la science qui étudie : 
                    <ol>
                        <li>Les conditions d’existence des êtres vivants </li> 
                        <li>Les interactions et relations existant entre les êtres vivants </li>
                        <li>Les interactions entre les êtres vivants et leur milieu = les écosytèmes</li>
                    </ol>
                <br/>
                La seconde définition de l'écologie est liée aux préoccupations environnementales liées aux évolutions climatiques, à la dégradation du cadre de vie local ou planétaire qu'elles soient dues à la pollution, au réchauffement climatique ou aux activités de l’homme. Cette utilisation du mot écologie vient de la naissance des mouvements écologiques dans les années 1960.
                Dans ce cadre l'écologie prend en compte l'action de l'homme sur son environnement afin d'en limiter les conséquences négatives et destructrices : pollution, destruction des écosystèmes, effet de serre, réchauffement de la planète, déforestation ...
                </p>
            </div>
            <div class="col-12 col-md-4">
                <img class="but_ecologie" src="img/but_ecologie.png">
            </div>
        </div>
    </div>
</section>
<section id="danger">
    <div class="container">
        <h2>La crise écologique, Quels sont les dangers ? </h2>
        <div class="row">
            <div class="col-12 col-md-4">
                <img class="imp_ecologie" src="img/imp_ecologie.png">
            </div>
            <div class="col-12 col-md-8">
                <p>La crise écologique ? C’est la conséquence des pollutions que l’activité humaine a créées.
                La plus connue est le réchauffement climatique. <br/><br/>
                Conséquence ? Les températures moyennes de la planète augmentent ce qui transforme l’écosystème global.  Mais aussi, l’extinction de la biodiversité (liée notamment à la destruction des habitats naturels), la pollution de l’air, la dégradation de la qualité des sols, la pollution des eaux, la surexploitation des ressources naturelles, la sur-pêche… <br/> Globalement, tous ces phénomènes se conjuguent et mettent en danger la stabilité des écosystèmes mondiaux, et donc notre capacité à vivre dans ces écosystèmes. 
                Aujourd’hui, le réchauffement climatique des conséquences très variées : il contribue à la fonte des glaces et à l’élévation du niveau de la mer. Il entraîne la multiplication des évènements météorologiques extrêmes et des catastrophes climatiques. Il transforme aussi l’agriculture en modifiant le métabolisme des plantes. <br/><br/>
                Résultat ? Le réchauffement climatique met en danger les sociétés humaines. </p>
            </div>
        </div>
    </div>
</section>

@endsection
