<!doctype html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
    <head>
        <meta charset="utf-8">
        <meta name="viewport" content="width=device-width, initial-scale=1">


        <title>L'écologie</title>

        <!-- Fonts -->
        <link href="https://fonts.googleapis.com/css?family=Nunito:200,600" rel="stylesheet">

        <!-- Styles -->
        <link href='css/app.css' rel='stylesheet' type='text/css'>
    </head>
    <body>
        
        <div class="accueil" id="home">
        <div class="background"></div>
        <div class="overlay"></div>
        <div class="main-content">
            <img class="logo" src="{{ asset('img/ecologie_blanc_300.png') }}">
            <div class="line"></div>
            <p>"Mieux vaux prendre le changement par la main avant qu'il nous prenne par la gorge"
            Wiston Churchill</p>
            <form>
                <input type="button" value="Tout savoir sur l'écologie" class="savoirplus" onclick="window.location.href='/definition' " />
            </form>
        </div>

            <!-- jQuery (necessary for Bootstrap's JavaScript plugins) -->
            <script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.0/jquery.min.js"></script>
            <script src="https://cdnjs.cloudflare.com/ajax/libs/jquery/2.1.3/jquery.min.js"></script>

            <!-- Include all compiled plugins (below), or include individual files as needed -->
            <script src="js/bootstrap.min.js"></script>

    </body>
</html>
