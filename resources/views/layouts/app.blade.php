<!DOCTYPE html>
<html lang="{{ str_replace('_', '-', app()->getLocale()) }}">
<head>
    <meta charset="utf-8">
    <meta name="viewport" content="width=device-width, initial-scale=1">

    <!-- CSRF Token -->
    <meta name="csrf-token" content="{{ csrf_token() }}">

    <title>@yield('title', config('app.name'))</title>
    <link rel="logo-icon" href="{{ asset('img/planet-earth.png') }}" />


    <!-- Scripts -->
    <script src="{{ asset('js/app.js') }}" defer></script>
    <script src="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/js/bootstrap.min.js" integrity="sha384-JjSmVgyd0p3pXB1rRibZUAYoIIy6OrQ6VrjIEaFf/nJGzIxFDsf4x0xIM+B07jRM" crossorigin="anonymous"></script>

    <!-- Fonts -->
    <link rel="dns-prefetch" href="//fonts.gstatic.com">
    <link href="https://fonts.googleapis.com/css?family=Nunito" rel="stylesheet">

    <!-- Styles -->
    <link href='css/app.css' rel='stylesheet' type='text/css'><link rel="stylesheet" href="https://stackpath.bootstrapcdn.com/bootstrap/4.3.1/css/bootstrap.min.css" integrity="sha384-ggOyR0iXCbMQv3Xipma34MD+dH/1fQ784/j6cY/iJTQUOhcWr7x9JvoRxT2MZw1T" crossorigin="anonymous"> 
        
</head>
<body>
    <div id="app">
        <nav>
            <div id="nav-wrapper">
                <div class="nav-left">
                <a href="./index.php"><img src="img/ecologie_blanc_100.png"></a>
                </div>
                <div class="nav-right">
                    <ul>
                        <li><a class="smooth-scroll" href="/definition">Définition</a>
                            <ul class="sub-nav">
                            <li><a href="/definition#but">l'écologie, Qu'est ce que c'est et à quoi ça sert?</a></li>
                            <li><a href="/definition#danger">la crise écologique, Quels sont les dangers?</a></li>
                            </ul>
                        </li>                  
                        <li><a class="smooth-scroll" href="/astuce">Astuces</a></li>
                        <li><a class="smooth-scroll" href="/actualite">Actualité</a></li>
                        <li><a class="smooth-scroll" href="/association">Associations</a></li>
                    </ul>
                </div>
            </div>
        </nav>
        <main class="py-4">
            @yield('content')
        </main>
    </div>
</body>
<footer>

<!-- Footer -->
<footer>
    <div class="container">
        <div class="row text-center justify-content-center pt-3 sb-2">
            <div class="col-md-2 sb-3">
                <h6>
                <a class=liens href="./index.php">Accueil</a>
                </h6>
            </div>
            <div class="col-md-2 sb-3">
                <h6>
                <a class=liens href="/definition">Définitions</a>
                </h6>
            </div>
            <div class="col-md-2 sb-3">
                <h6>
                <a class=liens href="/astuce">Astuces</a>
                </h6>
            </div>
            <div class="col-md-2 sb-3">
                <h6>
                <a class=liens href="/actualite">Actualités
                </a>
                </h6>
            </div>
            <div class="col-md-2 sb-3">
                <h6>
                <a class=liens href="/association">Associations</a>
                </h6>
            </div>
        </div>
        <div class="footer-copyright text-center">© 2019 Copyright:
            <a class=liens href=""> Coralie Debruyne</a>
        </div>
    </div>
</footer>
</html>
