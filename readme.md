Nom du projet: l'écologie

git clone https://gitlab.com/coraliedebruyne0/tfe.git

composer install

1. php -r "copy('https://getcomposer.org/installer', 'composer-setup.php');"
2. php -r "if (hash_file('sha384', 'composer-setup.php') === 'a5c698ffe4b8e849a443b120cd5ba38043260d5c4023dbf93e1558871f1f07f58274fc6f4c93bcfd858c6bd0775cd8d1') { echo 'Installer verified'; } else { echo 'Installer corrupt'; unlink('composer-setup.php'); } echo PHP_EOL;"
3. php composer-setup.php
4. php -r "unlink('composer-setup.php');"

installer l'app
1. rename env -> .env
4. php artisan migrate

créer une base de donnée
1. ouvrir Laragon 
2. start all
3. clic droit  - mySQL
                - créer base de donnée
                - tfe

git push