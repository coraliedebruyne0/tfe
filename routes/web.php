<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});
Route::get('definition', function()
{
    return View::make('pages.definition');
});
Route::get('actualite', function()
{
    return View::make('pages.actualite');
});
Route::get('astuce', function()
{
    return View::make('pages.astuce');
});

Route::get('association', function()
{
    return View::make('pages.association');
});


Auth::routes();

Route::get('/home', 'HomeController@index')->name('home');
Route::resource('definition', 'DefinitionController');
Route::resource('astuce', 'AstuceController');
Route::resource('actualite', 'ActualiteController');
Route::resource('association', 'AssociationController');

Route::group(['prefix' => config('backpack.base.route_prefix', 'admin'), 'middleware' => ['web', 'auth'], 'namespace' => 'Admin'], function () {
    // Backpack\NewsCRUD
    CRUD::resource('article', 'ArticleCrudController');
    CRUD::resource('category', 'CategoryCrudController');
    CRUD::resource('tag', 'TagCrudController');
});
